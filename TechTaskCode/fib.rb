#!/usr/bin/env ruby
require 'pry'
class Fib
  def evaluate(num)
    a = 0
    b = 1
    num.times do
      #a, b = b, a + b
      a = b
      b = a + b
      binding.pry
    end
    a
  end
end


if $0 == __FILE__
  puts Fib.new.evaluate(10)
end
