#!/usr/bin/env ruby
require 'minitest/autorun'
require 'minitest/pride'

require_relative "fizz_buzz"

describe FizzBuzz do
  before do 
    @fb = FizzBuzz.new(9,15)
  end
  
  it "Should evaluate to the correct array when calling evaluate" do
    @fb.evaluate.must_equal ["Fizz","Buzz",11,"Fizz",13,14,"FizzBuzz"]
  end
end