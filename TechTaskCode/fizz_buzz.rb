#!/usr/bin/env ruby
require 'pry'
class FizzBuzz
  def initialize(lower_bound,upper_bound)
    @lower_bound = lower_bound
    @upper_bound = upper_bound
  end
  
  def evaluate
    (@lower_bound..@upper_bound).map do |i|
      #binding.pry
      if(i % 3 == 0 && i % 5 == 0)
        "FizzBuzz"
      elsif (i%3 == 0)
        "Fizz"
      elsif (i%5 == 0)
        "Buzz"
      else
        i
      end
    end
  end 
end
def main
  puts FizzBuzz.new(ARGV[0].to_i,ARGV[1].to_i).evaluate
end
if $0 == __FILE__
  main
end