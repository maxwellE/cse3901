#!/usr/bin/env ruby
require 'minitest/autorun'
require 'minitest/pride'

require_relative "fib"

describe Fib do
  it "Fib(10) should equal to 55" do
    Fib.evaluate(10).must_equal 55
  end
end
