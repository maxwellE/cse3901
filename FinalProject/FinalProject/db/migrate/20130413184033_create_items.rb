class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.integer :high
      t.integer :low
      t.integer :chance_of_rain
      t.integer :category
      t.string  :title
      t.string  :image

      t.timestamps
    end
  end
end
