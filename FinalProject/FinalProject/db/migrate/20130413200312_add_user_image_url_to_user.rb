class AddUserImageUrlToUser < ActiveRecord::Migration
  def change
    add_column :users, :user_image_url, :string
  end
end
