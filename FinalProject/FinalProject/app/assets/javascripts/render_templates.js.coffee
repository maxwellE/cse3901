$ ->
  $('#header').html(HandlebarsTemplates['header']({low:$.cookie('low'),high:$.cookie('high'),rain_chance:$.cookie('rain')}))
  $.ajax '/items/user_and_approved_items_index',
    data: 
      high: $.cookie('high'),
      low: $.cookie('low'),
      rain: $.cookie('rain'),
    success: (data,status,xhr) -> 
      # [["Headwear", 0], ["Upper body", 1], ["Lower body", 2], ["Accessory", 3]]
      if data.length > 0
        $('#items').html HandlebarsTemplates['items']({headwear:data[0],upper_body:data[1],lower_body:data[2],accessory:data[3]})
      else
        $("#items").html "<p class='lead'>Could not find any items that fit the current weather! If you like you can add some items with the button on the top right</p>"
    error: (xhr,status,err) ->
      $('#items').html "<p class='lead'>There was an error while contacting the server, please reload the page to try again.</p>"
