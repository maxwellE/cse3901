(function(){
  var $weather_key = "943a559a3048350a";
  var lat = $.cookie('lat')
    , lng = $.cookie('lng')
    , city = $.cookie('city')
    , state = $.cookie('state')
    , high = $.cookie('high')
    , low = $.cookie('low')
    , rain = $.cookie('rain')

  if(lat && lng){
    if(!(city && state)){
      getLocation({coords: {
        latitude: lat,
        longitude: lng,
      }});
    }else if(!(high && low && rain)){
      getConditions(city, state);
    }
  }else{
    if(navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(getLocation, errorCor, {maximumAge:60000, timeout:5000, enableHighAccuracy:true});
    }
  }

  function errorCor() { }

  function getLocation(position) {
    var coords = position.coords || google.loader.ClientLocation;
    $.cookie('lat', coords.latitude);
    $.cookie('lng', coords.longitude);
    $.getJSON('http://api.wunderground.com/api/'+$weather_key+'/geolookup/q/'+coords.latitude+','+coords.longitude+'.json?callback=?', function(response) {
      getConditions(response.location.state, response.location.city);
    });
  }

  function getConditions(state, city_name) {
    $.getJSON('http://api.wunderground.com/api/'+$weather_key+'/forecast/q/'+state+'/'+city_name+'.json?callback=?', function(response) {
      var forecast = response.forecast.simpleforecast.forecastday[0];
      $.cookie('low', forecast.low.fahrenheit);
      $.cookie('high', forecast.high.fahrenheit);
      $.cookie('rain', forecast.pop);
    });
  }
  
})();
