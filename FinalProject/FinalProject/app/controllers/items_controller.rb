class ItemsController < ApplicationController

  before_filter :logged_in?, only: [:new, :create]
  before_filter :admin?, only: [:approve, :destroy, :index]

  def admin?
    if not (current_user and current_user.admin?)
      flash[:notice] = "WHY you no admin!!"
      redirect_to :root
    end
  end

  def logged_in?
    unless current_user
      flash[:notice] = "Please, log in"
      redirect_to :root
    end
  end
  
  def index
    @items = Item.where(:verified => false)
  end

  def new
    @item = Item.new
  end

  def destroy
    @item = Item.find(params[:id])
    @item.delete
    render :nothing => true
  end

  def approve
    @item = Item.find(params[:item_id])
    @item.verified = true
    @item.save
    render :json => true
  end

  def create
    @item = current_user.items.new(params[:item])
    if @item.save
      redirect_to root_path
    else
      render 'new'
    end
  end

  def user_and_approved_items_index
    passed_low = params[:low].to_i
    passed_high = params[:high].to_i
    passed_rain = params[:rain].to_i
    @items = current_user.items.where("? >= high OR ? >= low",passed_high,passed_low) | Item.where(:verified => true).where("? >= high OR ? >= low",passed_high,passed_low)
    Rails.logger.debug @items.to_json
    render :json => @items.group_by{|x| x.category}
  end
end
