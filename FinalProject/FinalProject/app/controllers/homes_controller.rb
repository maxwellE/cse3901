class HomesController < ApplicationController
  def index
    unless current_user
      return redirect_to :root
    end
  end

  def landing
    if current_user
      return redirect_to dashboard_path
    end
  end
end
