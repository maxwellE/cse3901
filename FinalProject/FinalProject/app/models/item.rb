class Item < ActiveRecord::Base
  belongs_to :user
  attr_accessible :category, :chance_of_rain, :high, :low, :verified,
                  :image, :title, :gender
  validates :image, :presence => true
  validates :title, :presence => true
end
