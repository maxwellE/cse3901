Project 3
======================
Group Members:
Maxwell Elliott, Web Scraping
Issac Post, Web Scraping
Maxim Kim, Web Scraping
Bob Carrol, Faculty Page
Haochi Chen, Faculty Page
Aaron Baker, Faculty Page

Tasks
--------------------

1. Web Scraping
------------------------
See 'web_scraping.sh' , the file is executable on unix systems. so using the
command

$ ./web_scraping.sh

should launch the scraper, A grep powered search of all software provided by
the OICO.

2. Web Page Redesign
----------------------
There are three pages for the faculty website: the home page (index.html), research, and professional activities. All three pages have successfully passed the W3 validator tests.
