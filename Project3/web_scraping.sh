#! /bin/bash
searcher(){
  echo -n "Enter search term to filter software, leave blank for master list: "
  read key
  bold=`tput bold`
  normal=`tput sgr0`
  if [ "$key" = "" ]; then
      key='.*'
      echo "${bold}All software offered by the OCIO:${normal}"
  else
      echo "${bold}Software matching search term '$key':${normal}"
  fi
  
  cat temp.out | sed ':a;N;$!ba;s/\n\|(\r\n)/1x2y3z/g' | sed 's/<\!doc.*<\/a><br\/><\/td>1x2y3z<\/tr><\/tbody><\/table>//g' | sed 's/<hr\/><p><strong>CONTACT.*<\/html>//g' | grep -P -o "<h2>(<strong>)?(<a id=\".*?\" name=\".*?\">(.*?)</a>)?(.*?)(.*?):?(</strong>)?</h2>" | sed s/\>['\s']+\</\>\</g | sed s/1x2y3z/'\n'/g  | sed s/:\s*\</\</g | sed s/\<[^\>]*\>//g | sed 's/^[\xc2\xa0 ]*//g' | sed 's/\&amp;/\&/g' | grep -i "$key"
}

run_again(){
  echo -n "Do you want to run another search?(y/n): "
  read again
  if [ "$again" = "y" ]
  then
    echo "======== New Search ========"
  else
    rm temp.out
    exit 0
  fi
}
curl http://ocio.osu.edu/software/directory/slwin/ 2> /dev/null > temp.out
echo -e "\033[41;37;5mSoftwareSearcher | Allows you to search the list of all software offered by the OCIO!\033[0m"

searcher
while run_again
do
  searcher
done
