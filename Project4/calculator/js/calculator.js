jQuery(document).ready(function($) {
    var add_to_display,
        set_display,
        contains_decimal,
        contains,
        contains_negative,
        prepend_display,
        current_eval_str,
        valid_key,
        memory_val,
        display_is_zero;
    memory_val = 0;
    current_eval_str = "";
    valid_key = function(key_code){
        if(48 >= key_code <= 57){
            return true;
        }else if(key_code == 46){
            return (!contains_decimal());
        }else{
            return false;
        }
    };
    display_is_zero = function(){
        return $('#main_display').val() === "0";
    };
    replace_char_in_display = function(char,replace_char){
        var current = $('#main_display').val();
        $('#main_display').val(current.replace(char,replace_char));  
    };
    set_display = function(digit){
        $('#main_display').val(digit);  
    };
    add_to_display = function(digit){
        $('#main_display').val($("#main_display").val() + digit); 
    };
    prepend_display = function(digit){
        $('#main_display').val(digit + $("#main_display").val()); 
    };
    contains = function(character){
        return $('#main_display').val().indexOf(character) != -1;
    };
    contains_decimal = function(){
        return contains('.');
    };
    contains_negative = function(){
        return contains('-');
    };
    $('button.digit').click(function() {
        if (display_is_zero()) {
            set_display(this.textContent);
        } else{
            add_to_display(this.textContent);
        }  
    });
    $("#clear").click(function(){
        $("#main_display").val("");
        current_eval_str = "";
    });
    $('#mem_save').click(function(){
        memory_val = parseFloat($('#main_display').val());
        $("#main_display").val("");
    });
    $('#mem_recall').click(function(){
      if(memory_val !== undefined && memory_val !== ""){
        $('#main_display').val(memory_val);
      }
    });
    $('#mem_add').click(function(){
       if(memory_val !== undefined && memory_val !== ""){
           if($('#main_display').val() !=="" && !isNaN(parseFloat($('#main_display').val()))){
               memory_val += parseFloat($('#main_display').val());
           }
       }
       $("#main_display").val("");
    });
    $('#mem_subtract').click(function(){
        if(memory_val !== undefined && memory_val !== ""){
             if($('#main_display').val() !=="" && !isNaN(parseFloat($('#main_display').val()))){
                memory_val -= parseFloat($('#main_display').val());
             }
         }
         $("#main_display").val("");
    });
    $("#mem_clear").click(function(){
        memory_val = 0;
    });
    $("#zero").click(function() {
        if(!display_is_zero()){
            add_to_display("0");
        }
    });
    $('#period').click(function(){
       if ($('#main_display').val() === "") {
           set_display("0.");
       } else {
           if (!contains_decimal()) {
               add_to_display(".");
           }
       }
    });
    $("#posneg").click(function(){
        if ($('#main_display').val() !== "" && $('#main_display').val() !== "0"){
            if(contains_negative()){
              replace_char_in_display('-','');
            }else{
                prepend_display('-');
            }
        }
    });
    $('.operator').click(function(){
         var operator = this.textContent;
         current_eval_str += ($('#main_display').val() + ' ' + operator);
         $("#main_display").val("");
    });
    $("#equal").click(function(){
         current_eval_str += ' ' + $('#main_display').val();
         $('#main_display').val(eval(current_eval_str));
         current_eval_str = "";
    });
    $("#main_display").on('keypress',function(e){ 
        if(e.which == 8){
            return true;
        }
        e.preventDefault();
        var new_str = ($('#main_display').val() + String.fromCharCode(e.which));
        if(new_str.match(/^\d*\.?\d*$/)){
            add_to_display(String.fromCharCode(e.which));
        }
    });
});
