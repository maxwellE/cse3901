The redesigned webpage is index.html, where you can find the added features for:

* A link that jumps to the next lecture.
* A button that folds and unfolds past lectures.

The web page includes the Date.js library for date parsing and handling, the jQuery and jQuery ScrollTo libraries for DOM traversing and manipulation.
The code that ties everything together can be found in the file project4.js.