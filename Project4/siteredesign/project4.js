$(function(){
  var lectures = $("#lectures");
  var lectures_rows = lectures.find("tr:not(:first)");
  var today = Date.today();
  var yesterday = new Date().add(-1).day();
  var counter = 0;
  
  lectures.find("tr:not(:first)").each(function(){
    var row = $(this);
    var row_date_str = $(this).find("td").eq(2).text();
    var row_date = Date.parse(row_date_str);
    if(row_date.isBefore(today)){
      counter++;
    }
  });
  
  $("#toggle").click(function(){
    if(lectures.find("tr:not(:visible)").length == 0){ // if nothing is hidden, we make toggling hide stuff
      lectures_rows.slice(0, counter).hide();
      $(this).text("Unfold");
    }else{ // else, show all rows
      lectures_rows.show();
      $(this).text("Fold");
    }
  });
  
  $("#next-meeting").click(function(){
    $(window).scrollTo(lectures_rows.eq(counter));
    return false;
  });
});