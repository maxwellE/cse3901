var stateRep = [0, 0, 0, 0, 0, 0, 0, 0, 0]
var userVal = 1
var compVal = 10
var emptVal = 0;

function userFillIn(space)
{
	if (!isFilled(space))
	{
		document.getElementById("btn"+space).innerHTML = "X";
		stateRep[space] = userVal

		if(score(stateRep) == -1)
		{
			alert("You won")
			location.reload()
			return
		}
		var first = findFirst(stateRep)
		if(score(stateRep) == 0 && findFirst(stateRep) === false)
		{
			alert("Tie")
			location.reload()
			return
		}

		var vwin = mwin(stateRep, compVal)

		if(vwin === false)
		{

			vwin = mwin(stateRep, userVal)

			if(vwin === false)
			{
				vwin = findFirst(stateRep)
			}
			stateRep[vwin] = compVal 
			cpuFillIn(vwin)	
		}
		else
		{
			stateRep[vwin] = compVal 
			cpuFillIn(vwin)
			alert("I won")
			location.reload()
			return
		}
	}
}
function cpuFillIn(space)
{
	document.getElementById("btn"+space).innerHTML = "O";
}

function mhint(hint)
{
	document.getElementById("hint").innerHTML += hint + "</br>";
}

function isFilled(space)
{
	var content = document.getElementById("btn"+space);
	if (content.innerHTML == "")
	{
		return false;
	}
	return true;
}

function copyArray(marray)
{
	var newarray = new Array()
	for (var i = 0; i < marray.length; i++) {
		newarray[i] = marray[i]
	};
	return newarray
}

function findFirst(mstate)
{
	var emptArr = new Array()
	for (var i = 0; i < mstate.length; i++) {
		if(mstate[i] == emptVal)
		{
			emptArr.push(i)
		}
	};
	if(emptArr.length == 0)
	{
		return false
	}
	var randChoice = Math.floor( (Math.random()*(emptArr.length-1)+1)  );
	return emptArr[randChoice];
}

function othersrow(pos)
{
	switch(pos)
	{
		case 0:
		return [1, 2]
		case 1:
		return [0, 2]
		case 2:
		return [0, 1]
		case 3:
		return [4, 5]
		case 4:
		return [3, 5]
		case 5:
		return [3, 4]

		case 6:
		return [7, 8]
		case 7:
		return [6, 8]
		case 8:
		return [6, 7]
		default:
		return false
	}
}

function otherscol(pos)
{
	switch(pos)
	{
		case 0:
		return [3, 6]
		case 1:
		return [4, 7]
		case 2:
		return [5, 8]

		case 3:
		return [0, 6]
		case 4:
		return [1, 7]
		case 5:
		return [2, 8]

		case 6:
		return [0, 3]
		case 7:
		return [1, 4]
		case 8:
		return [2, 5]
		default:
		return false
	}
}

function mwin(mstate, compVal) 
{
    // 0 1 2
    // 3 4 5
    // 5 7 8
    // var compVal = 10; 
    // var userVal = 1;
    // var emptVal = 0;
    var centerPos = 4


    for (var i = 0; i < mstate.length; i++) {

    	var oths = othersrow(i)

		// document.write(oths + "</br>")
		if(mstate[oths[0]]==compVal && mstate[oths[1]]==compVal && mstate[i]==emptVal)
		{
			return i 
		}

		oths = otherscol(i)

		if(mstate[oths[0]]==compVal && mstate[oths[1]]==compVal && mstate[i]==emptVal)
		{
			return i 
		}

	};

	if(mstate[centerPos] == emptVal)
	{
		if(mstate[0]==compVal && mstate[8]==compVal)
		{
			return centerPos
		}

		if(mstate[2]==compVal && mstate[6]==compVal)
		{
			return centerPos
		}
	}

    // 0 1 2
    // 3 4 5
    // 6 7 8

    if(mstate[centerPos]==compVal)
    {

    	if(mstate[0] == compVal && mstate[8] == emptVal)
    	{
    		return 8	
    	}

    	if(mstate[2] == compVal && mstate[6] == emptVal)
    	{
    		return 6	
    	}

    	if(mstate[6] == compVal && mstate[2] == emptVal)
    	{
    		return 2	
    	}

    	if(mstate[8] == compVal  && mstate[0] == emptVal)
    	{
    		return 0	
    	}
    }

    return false
}


function score(marray)
{
	var rowsums = new Array()
	var compVal = 10
	var playerVal = 1

	var tempsum = 0

	for (var i = 0; i < marray.length; i+=3) {
		tempsum = 0;
		for (var j = i; j < i + 3; j++) {
			tempsum += marray[j]
		};
		rowsums.push(tempsum)
	};


	for (var i = 0; i < 3; i++) {
		tempsum = 0;
		for (var j = i; j < marray.length; j+=3) {
			tempsum += marray[j]
		};
		rowsums.push(tempsum)
	};

	tempsum = 0
	tempsum = marray[0] + marray[4] + marray[8]
	rowsums.push(tempsum)
	tempsum = 0
	tempsum = marray[2] + marray[4] + marray[6]
	rowsums.push(tempsum)
	console.log(rowsums)

	if(rowsums.indexOf(3) != -1)
	{
		return -1
	}

	if(rowsums.indexOf(30) != -1)
	{
		return 1 
	}

	return 0
}

