Project 5
================
Groups Members
---------------
Haochi Chen: Web API
Maxim Kim: Web API
Aaron Baker: Web API
Issac Post: SudokuSolver
Maxwell Elliott: SudokuSolver

Mini Mashup
-------------
See README.txt in the mash-up folder

Sudoku Solver
------------
See README.txt in the SudokuSolver folder

Rails
------
See screenshots folder, each member has a screenshot with their name include on the image file name.
