The mash up uses the songkick API to retrieve a given artist's upcoming gigs, and then plots the location on a map using the Google Maps API. You can find a demo by opening the index.html file. The application has been tested in Firefox.

An example artist can be 'Beyonce'.
