gem 'minitest'
require 'minitest/autorun'
require 'minitest/spec'
require 'minitest/pride'
require 'pry'
require './sudoku_solver'
#require_relative "sudoku_solver"

describe SudokuSolver do
  before do 
    @ss = SudokuSolver.new("difficult1_grid")
  end

  it "should find the valid solution" do
    @ss.generate_output_string.must_equal File.read("difficult1_solution")
  end

end
