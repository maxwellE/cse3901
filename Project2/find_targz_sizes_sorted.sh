#!/bin/bash
echo "Filename : Size in Bytes (sorted descending)"
find . -regex .*\.tar\.gz$ -ls | sort | awk '{printf "%s : %s\n",$11,$7}'
