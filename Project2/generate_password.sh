#!/bin/bash
function get_word(){
  local word=$1
  echo $(grep "^$word\b" corrected_dictionary.txt | awk '{print $2}')
}
echo "5 random words selected from 'corrected_dictionary.txt':"
echo ""
while read -a line; do
    get_word ${line[0]}
done < <(curl --silent "http://www.random.org/integers/?num=5&min=1&max=7490&col=1&base=10&format=plain&rnd=new")
echo ""
echo "Use these words to create a strong password!"

