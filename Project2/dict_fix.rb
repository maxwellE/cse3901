#!/usr/bin/env ruby -wKU
lines = IO.readlines("dictionary.txt") 
File.open("corrected_dictionary.txt","w+") do |fh|
  index = 0
  lines.each do |e|
    actual_element = e.split(/\s+/).last
    next if actual_element =~ /\A(\d+|\d+(th|nd|rd|st))\z/
    fh.puts "#{index+=1} #{e.split(/\s+/).last}"
  end
end